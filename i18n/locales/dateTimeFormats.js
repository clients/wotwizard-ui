const objTime = {
	short: {
		day: "2-digit",
		month: "2-digit",
		year: "2-digit"
	},
	long: {
		day: "numeric",
		month: "long",
		year: "numeric"
	},
	hour: {
		hour: "numeric"
	},
	min: {
		minute: "2-digit"
	},
	time: {
		hour: "numeric",
		minute: "2-digit",
		hour12: false
	}
}
export const dateTimeFormats = {
	de: objTime,
	en: objTime,
	es: objTime,
	fr: objTime,
	it: objTime
}
