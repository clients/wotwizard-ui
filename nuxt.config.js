const i18n = require("./i18n");
const endpoints = require('./endpoints.json');

export default {
	// Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
	ssr: false,

	// Target: https://go.nuxtjs.dev/config-target
	target: "static",

	// Variables that can be accessed by $config
	publicRuntimeConfig: {},

	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: "wotwizard-ui",
		meta: [
			{ charset: "utf-8" },
			{ name: "viewport", content: "width=device-width, initial-scale=1" },
			{ hid: "description", name: "description", content: "" },
			{ name: "format-detection", content: "telephone=no" }
		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/favicon-96x96.png" }]
	},

	// Global CSS: https://go.nuxtjs.dev/config-css
	css: ["@/assets/css/style.scss"],

	// Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
	plugins: [
		"~plugins/favourites.js",
		"~plugins/filters.js",
		"~plugins/bootstrap.js",
		"~plugins/getApolloClient.js"
	],

	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,

	// Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
	buildModules: [
		// https://go.nuxtjs.dev/pwa
		"@nuxtjs/pwa",
		// https://github.com/whardier/nuxt-hero-icons
		"@nuxt-hero-icons/solid/nuxt"
	],

	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		// https://i18n.nuxtjs.org
		"@nuxtjs/i18n",
		// https://github.com/nuxt-community/apollo-module
		"@nuxtjs/apollo"
	],

	i18n: {
		lazy: true,
		langDir: "~/i18n/locales/",
		defaultLocale: "en",
		strategy: "prefix",
		locales: [
			{
				code: "en",
				name: "English",
				file: "en.json"
			},
			{
				code: "it",
				name: "Italiano",
				file: "it.json"
			},

			{
				code: "fr",
				name: "Français",
				file: "fr.json"
			},
			{
				code: "es",
				name: "Español",
				file: "es.json"
			},
			{
				code: "de",
				name: "Deutsch",
				file: "de.json"
			}
		],
		detectBrowserLanguage: {
			alwaysRedirect: true,
			fallbackLocale: "en"
		},
		vueI18n: i18n
	},

	// PWA module configuration: https://go.nuxtjs.dev/pwa
	pwa: {
		icon: {
			purpose: "any"
		},
		manifest: {
			name: "Wotwizard",
			short_name: "Wotwizard",
			description:
				"Vérifiez les entrées et les sorties de la toile de confiance de la monnaie libre Ğ1",
			lang: "fr",
			background_color: "#343a40",
			shortcuts: [
				{ name: "Favoris", url: "/favoris" },
				{ name: "Futurs membres", url: "/previsions" },
				{ name: "Lexique", url: "/lexique" },
				{ name: "Accueil", url: "/" }
			]
		}
	},

	apollo: {
		clientConfigs: Object.keys(endpoints).reduce((configs, key) => {
			configs[key] = `~/graphql/endpoints/${key}.js`;
			return configs;
		}, {}),
	},

	router: {
		linkExactActiveClass: "active"
	},

	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: {
		extend(config, ctx) {
			if (ctx && ctx.isClient) {
				config.optimization.splitChunks.maxSize = 243000
			}
		}
	}
}
