import {
	InMemoryCache,
	IntrospectionFragmentMatcher,
	defaultDataIdFromObject
} from "apollo-cache-inmemory"
import introspectionQueryResultData from "./fragmentTypes.json"

const fragmentMatcher = new IntrospectionFragmentMatcher({
	introspectionQueryResultData
})

// Apparemment il faut utiliser la syntaxe Apollo v2
export const cache = new InMemoryCache({
	addTypename: false,
	fragmentMatcher,
	dataIdFromObject: (object) => {
		switch (object.__typename) {
			case "Identity":
				return object.hash
			case "Event":
				return object.block.number
			case "EventId":
				return `${object.member.hash}:${object.inOut}`
			case "Forecast":
				return `${object.member.hash}:${object.date}:${object.after}:${object.proba}`
			case "GroupId":
				return `${object.id.hash}`
			case "CertHist":
				return `${object.id.uid}:${object.hist[0].block.number}`
			case "CertEvent":
				return `${object.in}:${object.block.number}`
			case "Block":
				return `${object.number}`
			default:
				return defaultDataIdFromObject(object) // fall back to default handling
		}
	}
})
