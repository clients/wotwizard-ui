import gql from "graphql-tag"

export const VERSION = gql`
	query Version {
		version
	}
`

// Pour la sidebar
export const LAST_BLOCK = gql`
	query LastBlock {
		countMax {
			number
			bct
			utc0
		}
	}
`

// Pour la page index
export const LAST_EVENTS = gql`
	query LastEvents($start: Int64, $end: Int64) {
		membersCount(start: $start, end: $end) {
			idList {
				__typename
				member: id {
					__typename
					pubkey
					uid
					status
					hash
					minDatePassed
					minDate
					certsLimit
					limitDate
					quality {
						__typename
						ratio
					}
					history {
						__typename
						in
						block {
							__typename
							number
						}
					}
					sent_certifications {
						__typename
						pending
					}
				}
				inOut
			}
			block {
				__typename
				number
			}
			number
		}
	}
`

// Pour la page previsions/newcomers
export const NEWCOMERS = gql`
	query GetNewcomers {
		wwResult {
			__typename
			permutations_nb
			dossiers_nb
			certifs_nb
			forecastsByNames {
				__typename
				member: id {
					__typename
					pubkey
					uid
					status
					hash
					certsLimit
					limitDate
				}
				date
				after
				proba
			}
		}
	}
`

// Pour la page membre
export const SEARCH_MEMBERS = gql`
	query SearchMember($hint: String) {
		idSearch(with: { hint: $hint }) {
			__typename
			ids {
				__typename
				pubkey
				uid
				status
				minDate
				minDatePassed
				hash
				certsLimit
				limitDate
				quality {
					__typename
					ratio
				}
				sent_certifications {
					__typename
					pending
				}
			}
		}
	}
`

// Pour la page membre
export const SEARCH_MEMBER = gql`
	query SearchMemberWithHash($hash: Hash!) {
		idFromHash(hash: $hash) {
			...attr
			pubkey
			isLeaving
			sentry
			membership_pending
			all_certifiersIO {
				...IO
			}
			all_certifiedIO {
				...IO
			}
			distanceE {
				__typename
				value {
					__typename
					ratio
				}
				dist_ok
			}
			distance {
				__typename
				value {
					__typename
					ratio
				}
				dist_ok
			}
			received_certifications {
				__typename
				from {
					...attr
				}
				expires_on
				pending
			}
			sent_certifications {
				__typename
				to {
					...attr
				}
				expires_on
				pending
			}
		}
	}
	fragment attr on Identity {
		__typename
		uid
		hash
		pubkey
		status
		certsLimit
		limitDate
		minDate
		minDatePassed
		quality {
			__typename
			ratio
		}
		sent_certifications {
			__typename
			pending
		}
	}
	fragment IO on CertHist {
		__typename
		id {
			...attr
		}
		hist {
			__typename
			in
			block {
				__typename
				number
				utc0
			}
		}
	}
`

// Pour la page parametres
export const PARAMS = gql`
	query getParams {
		allParameters {
			name
			par_type
			value
			comment
		}
	}
`
// Pour la page des suivis
export const MEMBERS = gql`
	query getMembers($group: [String!]!) {
		filterGroup(group: $group) {
			__typename
			selected {
				__typename
				id {
					...attr_fav
				}
			}
			others {
				__typename
				id {
					...attr_fav
				}
			}
		}
	}
	fragment attr_fav on Identity {
		__typename
		pubkey
		uid
		status
		hash
		certsLimit
		limitDate
		minDatePassed
		minDate
		quality {
			__typename
			ratio
		}
		sent_certifications {
			__typename
			pending
		}
	}
`

// Pour la page previsions/futures_sorties
export const NEXT_EXITS = gql`
	query NextExits($group: [String!], $start: Int64, $period: Int64) {
		memEnds(group: $group, startFromNow: $start, period: $period) {
			__typename
			pubkey
			uid
			status
			hash
			minDatePassed
			minDate
			certsLimit
			limitDate
			quality {
				__typename
				ratio
			}
			sent_certifications {
				__typename
				pending
			}
		}
	}
`
// Pour la page previsions/futures_sorties
export const NEXT_LOOSE_CERTS = gql`
	query NextLoseCerts(
		$group: [String!]
		$start: Int64
		$period: Int64
		$missingIncluded: Boolean
	) {
		certEnds(
			group: $group
			startFromNow: $start
			period: $period
			missingIncluded: $missingIncluded
		) {
			__typename
			pubkey
			uid
			status
			hash
			minDatePassed
			minDate
			certsLimit
			limitDate
			quality {
				__typename
				ratio
			}
			sent_certifications {
				__typename
				pending
			}
		}
	}
`
