
const { HttpLink } = require("apollo-link-http");
const { setContext } = require("apollo-link-context");
const { from } = require("apollo-link");
const { cache } = require("../cache");

const ssrMiddleware = setContext((_, { headers }) => {
    if (process.client) return headers;
    return { headers };
});

const httpLink = new HttpLink({ uri: "https://wotwizard.pini.fr/" });
const link = from([ssrMiddleware, httpLink]);

module.exports = () => ({
    link,
    cache,
    defaultHttpLink: false
});
