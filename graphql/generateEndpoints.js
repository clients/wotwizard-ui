const fs = require('fs');
const path = require('path');
const endpoints = require('../endpoints.json');

const endpointDir = path.join(__dirname, 'endpoints');

const recreateDirectory = (directory) => {
  if (fs.existsSync(directory)) {
    fs.rmSync(directory, { recursive: true, force: true });
  }

  fs.mkdirSync(directory, { recursive: true });
};

recreateDirectory(endpointDir);

const configTemplate = (uri) => `
const { HttpLink } = require("apollo-link-http");
const { setContext } = require("apollo-link-context");
const { from } = require("apollo-link");
const { cache } = require("../cache");

const ssrMiddleware = setContext((_, { headers }) => {
    if (process.client) return headers;
    return { headers };
});

const httpLink = new HttpLink({ uri: "${uri}" });
const link = from([ssrMiddleware, httpLink]);

module.exports = () => ({
    link,
    cache,
    defaultHttpLink: false
});
`;

Object.keys(endpoints).forEach(key => {
  const filePath = path.join(endpointDir, `${key}.js`);
  const fileContent = configTemplate(endpoints[key]);
  fs.writeFileSync(filePath, fileContent);
});
