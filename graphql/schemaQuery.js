const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const fs = require("fs")

fetch("https://gql.wotwizard.trentesaux.fr/", {
	method: "POST",
	headers: { "Content-Type": "application/json" },
	body: JSON.stringify({
		variables: {},
		query: `
      {
        __schema {
          types {
            kind
            name
            possibleTypes {
              name
            }
          }
        }
      }
    `
	})
})
	.then((result) => result.json())
	.then((result) => {
		const filteredData = result.data.__schema.types.filter(
			(type) => type.possibleTypes !== null
		)
		result.data.__schema.types = filteredData

		fs.writeFile(
			"./graphql/fragmentTypes.json",
			JSON.stringify(result.data),
			(err) => {
				if (err) {
					console.error("Error writing fragmentTypes file", err)
				} else {
					console.log("Fragment types successfully extracted!")
				}
			}
		)
	})
	.catch(function (error) {
		console.log(error.message)
	})
