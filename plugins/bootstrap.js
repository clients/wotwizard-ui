import Vue from "vue"
import Tooltip from "~/node_modules/bootstrap/js/dist/tooltip"
import Collapse from "~/node_modules/bootstrap/js/dist/collapse"
import Alert from "~/node_modules/bootstrap/js/dist/alert"

Vue.directive("tooltip-click", function (el, binding) {
	let tooltip = new Tooltip(el, {
		title: binding.value,
		html: true,
		trigger: "manual"
	})

	el.onclick = function () {
		tooltip.show()
		setTimeout(() => {
			tooltip.hide()
		}, 1000)
	}
})

Vue.directive("tooltip", function (el, binding, vnode) {
	if (binding.value.title != "") {
		let tooltip = new Tooltip(el, {
			title: binding.value.title,
			html: true,
			placement: binding.value.placement,
			trigger: "hover"
		})

		// vnode.context.$nextTick(() => {
		// 	var x = new MutationObserver(function (e) {
		// 		if (e[0].removedNodes) tooltip.hide()
		// 	})
		// 	x.observe(el.parentNode, { childList: true })
		// })
	}
})
