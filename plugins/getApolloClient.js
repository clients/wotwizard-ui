import Vue from "vue"

let mixin = {
	computed: {
		getApolloClient() {
			return localStorage.getItem("apollo-client") || "trentesaux"
		}
	}
}

Vue.mixin(mixin)
