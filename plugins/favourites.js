import Vue from "vue"

export default (context, inject) => {
	let liste_favoris = localStorage.favourites
		? JSON.parse(localStorage.favourites)
		: []

	let toggleFavourite = (uid, e) => {
		e.stopPropagation()
		if (!context.$favourites.list.includes(uid)) {
			context.$favourites.list.push(uid)
		} else {
			context.$favourites.list = context.$favourites.list.filter(
				(item) => item !== uid
			)
		}

		localStorage.favourites = JSON.stringify(context.$favourites.list)
	}

	let addFavorisArray = (certs, e) => {
		for (const cert in certs) {
			toggleFavourite(certs[cert].uid, e)
		}
	}

	let compileArrays = (allIO, actualIO) => {
		let all = allIO.map(function (CertHist) {
			return {
				...CertHist.id,
				dateout: CertHist.hist
					.filter((CertEvent) => !CertEvent.in)
					.reduce(
						function (prev, current) {
							return prev.block.utc0 > current.block.utc0 ? prev : current
						},
						{ block: { utc0: 0 } }
					).block.utc0
			}
		})

		all.forEach((certifier) => {
			let isFound = false

			for (const certif of actualIO) {
				if (certifier.uid == certif.uid) {
					isFound = true
					break
				}
			}

			if (!isFound && certifier.status != "REVOKED") {
				actualIO.push({
					__typename: "Certification",
					expires_on: certifier.dateout,
					...certifier,
					expired: true
				})
			}
		})

		return actualIO
	}

	let fixColumns = () => {
		let tables = document.querySelectorAll(".table-responsive table")

		tables.forEach((table) => {
			let colWidth = [...table.querySelectorAll("tbody tr:first-child td")].map(
				function (el) {
					return el.offsetWidth
				}
			)

			table.querySelectorAll("thead tr th").forEach((el, i) => {
				el.style.width = colWidth[i] - 0.5 + "px"
			})
		})
	}

	inject(
		"favourites",
		Vue.observable({
			list: liste_favoris,
			toggleFavourite: toggleFavourite,
			addFavorisArray: addFavorisArray,
			compileArrays: compileArrays,
			fixColumns: fixColumns
		})
	)
}
