#!/bin/bash

# Color for echo
RED='\e[31m'
GREEN='\e[32m'
NC='\e[0m'

TMP_FILE=temp
KEY_FILE=keysUsed

# Get all used keys in Vue files
grep --include=\*.vue --exclude=\*template.vue -roE -e "\\\$(t|tc)\((\"|')([a-zA-Z.]*)(\"|')(, [a-zA-Z._]*){0,1}\)" . > $TMP_FILE
sed -i -e "s/'/\"/" $TMP_FILE
sed -i -e "s/'/\"/" $TMP_FILE
sed -i -E -e 's/\..*:\$(t|tc)\("([a-zA-Z.]*).*$/\2/' $TMP_FILE

# Remove duplicates keys
sort $TMP_FILE | uniq > $KEY_FILE

# Verify all i18n files
FILES="i18n/locales/*.json"
RETURN_CODE=0
for f in $FILES
do
    echo -e "Processing ${GREEN}$f${NC} file..."
    
    tmp=$(mktemp)
    while read key; do
        # Verify if key exists
        jq -e ".$key" $f > /dev/null
        status=$?
        # If not, create it
        if [ $status -ne 0 ]
        then
            echo -e "create ${RED}$key${NC} in file ${GREEN}$f${NC}"
            value=$(jq -e ".$key" i18n/locales/fr.json)
            if [ $value = "null" ]
            then
                filter=".$key = \"TO_TRANSLATE\""
            else
                filter=".$key = $value"
            fi
            jq --sort-keys "$filter" $f > "$tmp" && mv "$tmp" $f
            RETURN_CODE=$((RETURN_CODE+1))
        fi
    done < $KEY_FILE
done

rm $KEY_FILE $TMP_FILE
# Return 0 if no key was created, or number of keys added
exit $RETURN_CODE
